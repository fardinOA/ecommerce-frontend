import React from "react";
import Footer from "./layout/Footer/Footer";

import New from "./layout/Header/New";

const AppLayout = (props) => {
    return (
        <>
            <New />
            <main>{props.children}</main>
            <Footer />
        </>
    );
};

export default AppLayout;
