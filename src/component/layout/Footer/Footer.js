import React from "react";
import "./Footer.css";
import playStore from "../../../images/playStore.png";
import appStore from "../../../images/appStore.png";
import { Link } from "react-router-dom";
function Footer({}) {
    const openInNewTab = (url) => {
        window.open(url, "_blank", "noreferrer");
    };
    return (
        <footer id="" className="   p-10  bg-[#0C1824] text-base-content">
            <div className="flex justify-around  space-y-10 lg:space-y-0  flex-col lg:flex-row">
                <div className="flex flex-col text-center ">
                    <span className="footer-title  text-[tomato] text-[1.4rem] ">
                        Services
                    </span>
                    <p className=" cursor-pointer text-[#D3E5A5]">Web Dev</p>
                    <p className=" cursor-pointer text-[#D3E5A5]">Frontend</p>
                    <p className=" cursor-pointer text-[#D3E5A5]">Backend</p>
                    <p className=" cursor-pointer text-[#D3E5A5]">Full Stack</p>
                </div>
                <div className="flex flex-col text-center ">
                    <span className="footer-title text-[tomato] text-[1.4rem] ">
                        Available For Select Freelance Opportunities
                    </span>
                    <p className=" cursor-pointer text-[#D3E5A5]"></p>

                    <p className=" cursor-pointer text-[#D3E5A5]">
                        Have an exciting project you need help with?
                    </p>
                    <p className=" cursor-pointer text-[#D3E5A5]">
                        Send me an email or contact me via instant message!
                    </p>
                </div>
                <div className="flex flex-col   ">
                    <span className="footer-title text-center text-[tomato] text-[1.4rem] ">
                        Contact
                    </span>
                    <b
                        role="link"
                        onClick={() =>
                            openInNewTab("https://fardin-me.vercel.app")
                        }
                        className="text-center text-[18px]   text-[#D3E5A5] cursor-pointer transition-all duration-300  "
                    >
                        Portfolio{" "}
                    </b>
                </div>
            </div>

            <div className="text-[#D3E5A5] flex justify-center border-t-2 mt-4 border-black ">
                <p>
                    <span className="text-[tomato]">Copyright © 2023</span> -
                    All right reserved by Fardin Omor Afnan
                </p>
            </div>
        </footer>
        // <footer id="footer">

        //     <div className="footerComponent">
        //         <ul>
        //             <li>
        //                 <Link to="/">Home</Link>
        //             </li>

        //             <li>
        //                 <Link to="/about">About</Link>
        //             </li>

        //             <li>
        //                 <Link to="/products">Products</Link>
        //             </li>

        //             <li>
        //                 <a
        //                     rel="noreferrer"
        //                     target="_blank"
        //                     href="mailto:afnan.eu.cse@gmail.com"
        //                 >
        //                     Contact
        //                 </a>
        //             </li>
        //         </ul>
        //         <div className="bottom">
        //             <span>
        //                 &copy; {new Date().getFullYear()}. All Right Reserved
        //             </span>
        //             <span>Designed &hearts; by Fardin Omor Afnan</span>
        //         </div>
        //     </div>
        // </footer>
    );
}

export default Footer;
