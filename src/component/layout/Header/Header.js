import { ReactNavbar } from "overlay-navbar";
import { CgProfile, CgShoppingCart, CgSearch } from "react-icons/cg";
import logo from "../../../images/logo.png";
import { Link } from "react-router-dom";
import "./Header.css";
import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import Tooltip from "@mui/material/Tooltip";
import MenuItem from "@mui/material/MenuItem";
import AdbIcon from "@mui/icons-material/Adb";
import { useState } from "react";

const pages = [
    { name: "Home", url: "/" },
    { name: "Products", url: "/products" },
    { name: "About", url: "/about" },
    { name: "Contact", url: "/contact" },
    {
        component: <CgSearch />,
        url: "/search",
    },
    { component: <CgShoppingCart />, url: "/cart" },
    { component: <CgProfile />, url: "/login" },
];
const options = {
    burgerColorHover: "#eb4034",
    logo,
    logoWidth: "2rem",
    navColor1: "white",
    logoHoverSize: "5.2rem",
    logoHoverColor: "#eb4034",
    link1Text: "Home",
    link2Text: "Products",
    link3Text: "Contact",
    link4Text: "About",
    link1Url: "/",
    link2Url: "/products",
    link3Url: "/contact",
    link4Url: "/about",
    link1Size: "1.5rem",
    link1Color: "rgba(35,35,35,0.8)",
    nav1justifyContent: "flex-end",
    nav2justifyContent: "flex-end",
    nav3justifyContent: "flex-start",
    nav4justifyContent: "flex-start",
    link1ColorHover: "#eb4034",
    link1Margin: "1rem",
    profileIcon: true,
    cartIcon: true,
    searchIcon: true,
    ProfileIconElement: CgProfile,
    CartIconElement: CgShoppingCart,
    SearchIconElement: CgSearch,
    profileIconUrl: "/login",
    profileIconColor: "rgba(35,35,35,0.8)",
    searchIconColor: "rgba(35,35,35,0.8)",
    cartIconColor: "rgba(35,35,35,0.8)",
    profileIconColorHover: "#eb4034",
    searchIconColorHover: "#eb4034",
    cartIconUrl: "/cart",
    cartIconColorHover: "#eb4034",
    cartIconMargin: "2rem",
};
const Header = () => {
    const [anchorElNav, setAnchorElNav] = useState(null);
    const [anchorElUser, setAnchorElUser] = useState(null);

    const handleOpenNavMenu = (e) => {
        setAnchorElNav(e.currentTarget);
    };
    const handleOpenUserMenu = (e) => {
        setAnchorElUser(e.currentTarget);
    };

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };
    return (
        <AppBar
            position="absolute"
            color="transparent"
            style={{
                zIndex: 47,
                boxSizing: "border-box",
            }}
        >
            <Container maxWidth="xl">
                <Toolbar disableGutters>
                    <Typography
                        variant="h6"
                        noWrap
                        component="a"
                        href="/"
                        sx={{
                            mr: 2,
                            display: { xs: "none", md: "flex" },
                            fontFamily: "monospace",
                            fontWeight: 700,
                            letterSpacing: ".3rem",
                            color: "inherit",
                            textDecoration: "none",
                        }}
                    >
                        <img
                            style={{ cursor: "pointer" }}
                            src="/logo.png"
                            alt="logo"
                        />
                    </Typography>

                    <Box
                        sx={{
                            flexGrow: 1,
                            // justifyContent: "flex-end",
                            display: { xs: "flex", md: "none" },
                        }}
                    >
                        <IconButton
                            size="large"
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleOpenNavMenu}
                            color="inherit"
                        >
                            <MenuIcon fontSize="20rem" />
                        </IconButton>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorElNav}
                            anchorOrigin={{
                                vertical: "bottom",
                                horizontal: "left",
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: "top",
                                horizontal: "left",
                            }}
                            open={Boolean(anchorElNav)}
                            onClose={handleCloseNavMenu}
                            sx={{
                                display: { xs: "block", md: "flex" },
                                flexDirection: { md: "column" },
                                zIndex: 48,
                            }}
                        >
                            {pages.map((page, ind) => (
                                <MenuItem
                                    key={ind}
                                    onClick={handleCloseNavMenu}
                                >
                                    <Typography
                                        style={{
                                            width: "100%",
                                            color: "black",
                                        }}
                                        textAlign="center"
                                    >
                                        <Link to={page.url}>
                                            {page.component
                                                ? page.component
                                                : page.name}
                                        </Link>
                                    </Typography>
                                </MenuItem>
                            ))}
                        </Menu>
                    </Box>

                    <Typography
                        variant="h5"
                        noWrap
                        component="a"
                        href=""
                        sx={{
                            mr: 2,
                            display: { md: "none" },
                            flexGrow: 1,
                            fontFamily: "monospace",
                            fontWeight: 700,
                            letterSpacing: ".3rem",
                            color: "inherit",
                            textDecoration: "none",
                        }}
                    >
                        <Link to="/">
                            <img
                                style={{ cursor: "pointer" }}
                                src="/logo.png"
                                alt="logo"
                            />
                        </Link>
                    </Typography>
                    <Box
                        sx={{
                            flexGrow: 1,
                            display: { xs: "none", md: "flex" },
                            justifyContent: "space-between",
                            marginRight: "5rem",
                            marginLeft: "5rem",
                        }}
                    >
                        {pages.map((page, ind) => (
                            <Button
                                key={ind}
                                onClick={handleCloseNavMenu}
                                sx={{ my: 4, color: "red", display: "block" }}
                            >
                                <Link to={page.url}>
                                    {page.component
                                        ? page.component
                                        : page.name}
                                </Link>
                            </Button>
                        ))}
                    </Box>
                </Toolbar>
            </Container>
        </AppBar>
    );
};
export default Header;
