import { motion } from "framer-motion/dist/framer-motion";
import React, { useEffect, useState } from "react";
import { CgProfile, CgShoppingCart, CgSearch, CgMenu } from "react-icons/cg";
import LOGO from "../../../images/logo2.png";
import { RiCloseCircleFill } from "react-icons/ri";
import { Link } from "react-router-dom";
const pages = [
    { name: "Home", url: "/" },
    { name: "Products", url: "/products" },
    { name: "About", url: "/about" },
    { name: "Contact", url: "/contact" },
    {
        component: <CgSearch />,
        url: "/search",
    },
    { component: <CgShoppingCart />, url: "/cart" },
    { component: <CgProfile />, url: "/login" },
];
const New = () => {
    const [modal, setModal] = useState(false);

    const [windowSize, setWindowSize] = useState(null);

    useEffect(() => {
        if (windowSize > 1023) setModal(false);
        const handleResize = () => {
            setWindowSize(window.innerWidth);
        };

        window.addEventListener("resize", handleResize);

        return () => window.removeEventListener("resize", handleResize);
    }, [windowSize]);

    return (
        <div className="container mx-auto relative px-10 mb-8 border-b-2 pb-8">
            <div className=" w-full flex justify-between border-white  ">
                <div className="  ">
                    <a href="/">
                        <img
                            className="w-28 absolute top-[-2rem] "
                            src={LOGO}
                            alt="logo"
                        />
                    </a>
                </div>
                <motion.div
                    animate={modal ? "open" : "closed"}
                    variants={{
                        open: { opacity: 1, x: 0 },
                        closed: {
                            opacity: 0,
                            x: "-100%",
                            animationDuration: 200,
                        },
                    }}
                    // active:scale-100
                    className={`    
                     
                    text-center 
                      absolute
                      inset-x-[8%]
                      top-[7rem]
                      z-[999999]
                      flex
                      flex-col
                      rounded-[18px]
                      bg-gray-50
                       transition
                       ease-out
                       duration-600
                        shadow-xl  
                       lg:!border-none
                      p-[4%]
                      px-[3%]
                      
                      lg:static
                      lg:!bg-transparent
                      lg:!inset-x-[0%]
                      lg:!top-[0%]
                      lg:z-[99999]
                      lg:w-[100%]
                      lg:flex-row
                      lg:justify-end
                      lg:right-0
                      lg:rounded-[0px]
                        
                      lg:p-[0%]
                      lg:px-[0%] ${!modal && "hidden   lg:contents "}`}
                >
                    {pages.map((ele, ind) => (
                        <Link
                            key={ind + 1}
                            onClick={() => setModal(false)}
                            to={ele.url}
                        >
                            <span
                                className="inline-flex 
                          items-center
                          flex-end 
                          p-[8px_2px] 
                           text-center
                          
                          text-lg 
                            cursor-pointer
                          capitalize 
                          font-[700]
                          leading-[1.22em] 
                           
                          transition-all 
                          duration-300 
                          ease-in-out 
                          hover:text-[tomato]
                           hover:-translate-y-1
                          "
                            >
                                {ele.component ? ele.component : ele.name}
                            </span>
                        </Link>
                    ))}
                </motion.div>
                <motion.div
                    whileTap={{
                        opacity: 1,
                        scale: 1.05,
                    }}
                    transition={{
                        type: "spring",
                        stiffness: 400,
                        damping: 10,
                    }}
                    className={`float-right lg:hidden `}
                >
                    {!modal ? (
                        <CgMenu
                            color="black"
                            onClick={() => setModal(!modal)}
                            className={`cursor-pointer `}
                            size={40}
                        />
                    ) : (
                        <RiCloseCircleFill
                            color="black"
                            onClick={() => setModal(!modal)}
                            className={`cursor-pointer `}
                            size={40}
                        />
                    )}
                </motion.div>
            </div>
            {/* <hr className="style18" /> */}
        </div>
    );
};

export default New;
