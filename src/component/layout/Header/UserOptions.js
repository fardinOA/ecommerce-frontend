import React, { Fragment, useState } from "react";
import "./Header.css";
import { SpeedDial, SpeedDialAction } from "@material-ui/lab";
import BackDrop from "@material-ui/core/Backdrop";
import DashboardIcon from "@material-ui/icons/Dashboard";
import PersonIcon from "@material-ui/icons/Person";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import ListAltIcon from "@material-ui/icons/ListAlt";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import { useHistory } from "react-router-dom";
import { useAlert } from "react-alert";
import { useSelector } from "react-redux";
import { logOut } from "../../../actions/userActions";
import store from "../../../store";
const UserOptions = ({ user }) => {
    const { cartItems } = useSelector((state) => state.cart);
    const [open, setOpen] = useState(false);
    const history = useHistory();
    const alert = useAlert();
    const dashboard = () => {
        history.push("/admin/dashboard");
    };
    const account = () => {
        history.push("/account");
    };
    const orders = () => {
        history.push("/orders");
    };
    const cart = () => {
        history.push("/cart");
    };
    const logout = () => {
        store.dispatch(logOut());

        alert.success("Logout success");
        history.push("/login");
    };
    return (
        <Fragment>
            <BackDrop style={{ zIndex: "49" }} open={open} />
            <SpeedDial
                className="speedDail"
                ariaLabel="SpeedDial tooltip example"
                open={open}
                onOpen={() => setOpen(true)}
                onClose={() => setOpen(false)}
                direction="down"
                style={{ zIndex: 100, backgroundColor: "transparent" }}
                icon={
                    <img
                        className="speedDialIcon"
                        src={user.avatar.url ? user.avatar.url : "/profile.png"}
                        alt="Profile"
                    />
                }
                FabProps={{
                    size: "large",
                    style: {
                        backgroundColor: "transparent",
                        boxShadow: "none",
                    },
                }}
            >
                {user.role === "admin" && (
                    <SpeedDialAction
                        icon={<DashboardIcon style={{ color: "#0F6358" }} />}
                        tooltipTitle="Dashboard"
                        onClick={dashboard}
                        tooltipOpen={window.innerWidth <= 600 ? true : false}
                    />
                )}
                <SpeedDialAction
                    icon={<PersonIcon style={{ color: "#0F6358" }} />}
                    tooltipTitle="Profile"
                    onClick={account}
                    tooltipOpen={window.innerWidth <= 600 ? true : false}
                />
                <SpeedDialAction
                    icon={
                        <ShoppingCartIcon
                            style={{
                                color:
                                    cartItems.length > 0 ? "tomato" : "#0F6358",
                            }}
                        />
                    }
                    tooltipTitle={`Cart(${cartItems.length})`}
                    onClick={cart}
                    tooltipOpen={window.innerWidth <= 600 ? true : false}
                />
                <SpeedDialAction
                    icon={<ListAltIcon style={{ color: "#0F6358" }} />}
                    tooltipTitle="Orders"
                    onClick={orders}
                    tooltipOpen={window.innerWidth <= 600 ? true : false}
                />
                <SpeedDialAction
                    icon={<ExitToAppIcon style={{ color: "#0F6358" }} />}
                    tooltipTitle="Logout"
                    onClick={logout}
                    tooltipOpen={window.innerWidth <= 600 ? true : false}
                />
            </SpeedDial>
        </Fragment>
    );
};

export default UserOptions;
