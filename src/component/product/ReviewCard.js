import React, { useEffect, useState } from "react";

import "./reviewCard.css";
import profilePng from "../../images/profilePng.png";
import { Rating } from "@material-ui/lab";
import { useDispatch } from "react-redux";
import axios from "axios";
import { DEFAULT_URL } from "../../constants/userConstants";
const ReviewCard = ({ review }) => {
    const options = {
        size: "large",

        readOnly: true,
        precision: 0.5,
        value: review?.rating,
    };
    const [user, setUser] = useState({});

    useEffect(() => {
        const getUser = async () => {
            try {
                const config = {
                    headers: {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Headers": "*",
                    },
                };
                axios.defaults.withCredentials = true;

                const { data } = await axios.get(
                    `${DEFAULT_URL}/api/v1/user/${review.user}`,
                    config
                );

                setUser(data?.user);
            } catch (error) {}
        };
        getUser();
    }, []);

    // console.log(review);
    return (
        <div key={review._id} className="reviewCard">
            <img
                className="rounded-full shadow-lg"
                src={`${
                    user?.avatar?.url ? `${user?.avatar?.url}` : `${profilePng}`
                }`}
                alt="User"
            />
            <p>{review?.name}</p>
            <Rating {...options} />
            <span>{review?.comment}</span>
        </div>
    );
};

export default ReviewCard;
