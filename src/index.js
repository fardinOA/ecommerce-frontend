import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { Provider } from "react-redux";
import store from "./store";
import { positions, transitions, Provider as AlertProvider } from "react-alert";
import AlertTemplate from "react-alert-template-basic";
import AppLayout from "./component/AppLayout";
import { AnimatePresence } from "framer-motion/dist/framer-motion";
const options = {
    timeout: 10000,
    position: positions.TOP_CENTER,
    transition: transitions.SCALE,
};
ReactDOM.render(
    <Provider store={store}>
        <AlertProvider template={AlertTemplate} {...options}>
            {/* <AppLayout> */}
            {/* <AnimatePresence> */} <App />
            {/* </AnimatePresence> */}
            {/* </AppLayout> */}
        </AlertProvider>
    </Provider>,
    document.getElementById("root")
);
